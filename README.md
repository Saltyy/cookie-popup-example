# Cookie Popup

#### Description
a simple example for a cookie popup
in this case we initialize Google Analytics and Google Tag Manager, when the cookies are accepted

#### Staging
_To apply changes, create merge requests and follow this order:_
 - custom branch    ->  dev
 - dev              ->  test
 - test             ->  master

#### Commit Rules
##### Commit "Keys"
	- add         - adding something new
	- update      - updating something existing
	- remove      - removing something unused. like a unneeded file

```sh
git commit -m '[Commit Key]:[Short Title]' -m '[Commit Message]'
```
##### examples:
```sh
    git commit -m 'fix: xy bug' -m 'added xy to solve bug xyz'
```
```sh
    git commit -m 'add: xy' -m 'added xy for new xy'
```
```sh
    git commit -m 'remove: xy' -m'remove file xy because its not needed anymore'
```

## Text Examples
```html 
	This site uses cookies to enhance your experience. Give 'em a go! Or don't! It's up to you!
	But, just so you know, our website won't work properly without some essential cookies.
	To gather info and make improvements, we also use third-party cookies. Can we? 
	<a href="<?php echo PRIVACY; ?>">Read more about our cookie ingredients (privacy policy)</a>.
```

```html 
	This site uses cookies to enhance your experience. Give 'em a go! Or don't! It's up to you!
	This website use cookie, please accept them. <a href="<?php echo PRIVACY; ?>"> Read more about our cookies~</a>
```

```html 
	We baked some cookies that you have to accept, if you want to enjoy this website.
	It simply doesn't work without. In order to gather information and make improvements, we need use some third-party cookies too. Can we?
	<a href="<?php echo PRIVACY; ?>"> Read more</a> about the ingredients if your allergic!
```

```html 
	We use essential cookies for this website to work properly. It won't function without them.
	To gather information and improve our services, we also use third-party cookies. Can we use these cookies?
	<a href="<?php echo PRIVACY; ?>"> Read more</a> about our ingredients (privacy policy)
```