/* ########################################################################
                            Cookie Popup
######################################################################## */

$(document).ready(function () {
    const cookieBox = document.querySelector(".cookie-popup");
    // "Accept all cookies"
    const acceptAllBtn = document.querySelector(".accept-cookies");
    // "Accept only essential cookies"
    const acceptEssentialBtn = document.querySelector(".accept-essential");
    // "Customize Cookies"
    const customizeBtn = document.querySelector(".check-options");
    // "Save And Apply"
    const saveChangesBtn = document.querySelector(".accept-customized");
    // the cookie on the right
    const editPreferencesIcon = document.getElementById("editCookiePreferencesIcon");

    // Customize Cookies 
    const cookieConfigurator = document.querySelector(".cookie-configurator");
    // Customize Cookies - "Functional Cookies"
    const functionalCookies = document.getElementById("functionalCookies");
    // Customize Cookies - "Analytics Cookies"
    const analyticsCookies = document.getElementById("analyticsCookies");
    // Customize Cookies - "Targeting/Advertising Cookies"
    const advertisingCookies = document.getElementById("advertisingCookies");
    // Customize Cookies - "Close"
    const closeConfiguratorBtn = document.querySelector(".close-configurator");
    // Customize Cookies - "Back"
    const backConfiguratorBtn = document.querySelector(".backToscrollableContent");

    // Function to initialize Google Analytics, Google Tag Manager, and Google Ads
    const initializeAnalyticsAndTagManagerAndAds = (functional, analytics, ads) => {
        // Replace 'YOUR_GA_TRACKING_ID' with your actual Google Analytics tracking ID
        const gaTrackingId = 'G-YOUR_GA_TRACKING_ID';
        // Replace 'YOUR_GTM_CONTAINER_ID' with your actual Google Tag Manager container ID
        const gtmContainerId = 'GTM-YOUR_GTM_CONTAINER_ID';
        // Replace 'YOUR_ADS_CLIENT_ID' with your actual Google Ads client ID
        const adsClientId = 'ca-pub-YOUR_ADS_TRACKING_ID';

        if (functional) {
            // Initialize functional scripts if required
        }

        if (analytics) {
            // Load Google Analytics
            (function() {
                var gtagScript = document.createElement('script');
                gtagScript.src = `https://www.googletagmanager.com/gtag/js?id=${gaTrackingId}`;
                gtagScript.async = true;
                document.head.appendChild(gtagScript);

                gtagScript.onload = function() {
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag('js', new Date());
                    gtag('config', gaTrackingId);
                };
            })();

            // Load Google Tag Manager
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', gtmContainerId);
        }

        if (ads) {
            // Load Google Ads
            (function() {
                var adsScript = document.createElement('script');
                adsScript.src = `https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js`;
                adsScript.async = true;
                document.head.appendChild(adsScript);

                adsScript.onload = function() {
                    (adsbygoogle = window.adsbygoogle || []).push({
                        google_ad_client: adsClientId,
                        enable_page_level_ads: true
                    });
                };
            })();

            // AdSense Ad Blocking Recovery
            (function() {
                var adsRecoveryScript = document.createElement('script');
                adsRecoveryScript.src = 'https://fundingchoicesmessages.google.com/i/pub-8533425235718576?ers=1';
                adsRecoveryScript.async = true;
                adsRecoveryScript.setAttribute('nonce', 'PWHB-_YHxqK3v8T3dSDKlg');
                document.head.appendChild(adsRecoveryScript);

                adsRecoveryScript.onload = function() {
                    (function() {
                        function signalGooglefcPresent() {
                            if (!window.frames['googlefcPresent']) {
                                if (document.body) {
                                    const iframe = document.createElement('iframe');
                                    iframe.style = 'width: 0; height: 0; border: none; z-index: -1000; left: -1000px; top: -1000px;';
                                    iframe.style.display = 'none';
                                    iframe.name = 'googlefcPresent';
                                    document.body.appendChild(iframe);
                                } else {
                                    setTimeout(signalGooglefcPresent, 0);
                                }
                            }
                        }
                        signalGooglefcPresent();
                    })();
                };
            })();
        }
    };

    // Function to set a cookie
    const setCookie = (name, value, days) => {
        const expires = new Date(Date.now() + days * 864e5).toUTCString();
        document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + expires + '; path=/';
    };

    // Function to get a cookie
    const getCookie = (name) => {
        return document.cookie.split('; ').find(row => row.startsWith(name + '='))?.split('=')[1];
    };

    // Function to handle cookie consent
    const handleCookieConsent = (allAccepted, essentialAccepted) => {
        if (allAccepted) {
            setCookie('analyticsConsent', 'accepted', 365);
            setCookie('functionalCookies', 'true', 365);
            setCookie('analyticsCookies', 'true', 365);
            setCookie('advertisingCookies', 'true', 365);
            initializeAnalyticsAndTagManagerAndAds(true, true, true);
        } else if (essentialAccepted) {
            setCookie('analyticsConsent', 'essential', 365);
            setCookie('functionalCookies', 'false', 365);
            setCookie('analyticsCookies', 'false', 365);
            setCookie('advertisingCookies', 'false', 365);
        } else {
            setCookie('analyticsConsent', 'custom', 365);
            setCookie('functionalCookies', functionalCookies.checked, 365);
            setCookie('analyticsCookies', analyticsCookies.checked, 365);
            setCookie('advertisingCookies', advertisingCookies.checked, 365);
            initializeAnalyticsAndTagManagerAndAds(functionalCookies.checked, analyticsCookies.checked, advertisingCookies.checked);
        }
        cookieBox.classList.remove("active");
        // Show the edit preferences icon once cookies are set
        editPreferencesIcon.style.display = 'block';
        backConfiguratorBtn.style.display = 'block';
        backConfiguratorBtn.style.display = 'none';
    };

    // Function to pre-toggle the checkboxes based on the stored cookie preferences
    const setInitialCookiePreferences = () => {
        const consentStatus = getCookie('analyticsConsent');
        if (consentStatus === 'accepted' || consentStatus === 'custom') {
            functionalCookies.checked = getCookie('functionalCookies') === 'true';
            analyticsCookies.checked = getCookie('analyticsCookies') === 'true';
            advertisingCookies.checked = getCookie('advertisingCookies') === 'true';
        }
    };

    // Call setInitialCookiePreferences on page load to ensure correct state
    setInitialCookiePreferences();

    // Check cookie consent status on page load
    const consentStatus = getCookie('analyticsConsent');
    if (consentStatus === 'accepted') {
        cookieBox.classList.remove("active");
        initializeAnalyticsAndTagManagerAndAds(true, true, true);
        editPreferencesIcon.style.display = 'block'; // Show the edit preferences icon
        closeConfiguratorBtn.style.display = 'block';
        backConfiguratorBtn.style.display = 'none';
    } else if (consentStatus === 'essential') {
        cookieBox.classList.remove("active");
        editPreferencesIcon.style.display = 'block'; // Show the edit preferences icon
    } else if (consentStatus === 'custom') {
        cookieBox.classList.remove("active");
        initializeAnalyticsAndTagManagerAndAds(functionalCookies.checked, analyticsCookies.checked, advertisingCookies.checked);
        editPreferencesIcon.style.display = 'block'; // Show the edit preferences icon
        closeConfiguratorBtn.style.display = 'block';
        backConfiguratorBtn.style.display = 'none';
    } else {
        cookieBox.classList.add("active");
        editPreferencesIcon.style.display = 'none'; // Hide the edit preferences icon
        closeConfiguratorBtn.style.display = 'none';
        backConfiguratorBtn.style.display = 'block';
    }

    // Event listeners for buttons
    if (acceptAllBtn) {
        acceptAllBtn.addEventListener("click", () => {
            handleCookieConsent(true, false);
        });
    }

    if (acceptEssentialBtn) {
        acceptEssentialBtn.addEventListener("click", () => {
            handleCookieConsent(false, true);
        });
    }

    if (customizeBtn) {
        customizeBtn.addEventListener("click", () => {
            cookieConfigurator.style.position = 'fixed';
            cookieConfigurator.style.top = `50%`;
            cookieConfigurator.style.left = `50%`;
            cookieConfigurator.style.transform = `translate(-50%, -50%)`;
            $('.cookie-configurator').addClass('active');
            $('.cookie-popup').removeClass('active');
            setInitialCookiePreferences();
        });
    }

    if (saveChangesBtn) {
        saveChangesBtn.addEventListener("click", () => {
            handleCookieConsent(false, false);
            $('.cookie-configurator').removeClass('active');
        });
    }

    // Event listener for the close button
    if (closeConfiguratorBtn) {
        closeConfiguratorBtn.addEventListener("click", () => {
            cookieConfigurator.style.position = 'fixed';
            cookieConfigurator.style.top = `50%`;
            cookieConfigurator.style.left = `50%`;
            cookieConfigurator.style.transform = `translate(-50%, -50%)`;
            $('.cookie-configurator').toggleClass('active');
            $('.cookie-popup').removeClass('active');
            setInitialCookiePreferences();
        });
    }

    // Event listener for the back button
    $(backConfiguratorBtn).click(function () {
        $('.cookie-configurator').removeClass('active');
        $('.cookie-popup').addClass('active');
    });

    // Event listener for expandable sections
    $('.expandableTitle').click(function () {
        const parent = $(this).closest('.expandableSection');
        parent.toggleClass('active');
    });

    if (editPreferencesIcon) {
        editPreferencesIcon.addEventListener("click", () => {
            cookieConfigurator.style.position = 'fixed';
            cookieConfigurator.style.top = `50%`;
            cookieConfigurator.style.left = `50%`;
            cookieConfigurator.style.transform = `translate(-50%, -50%)`;
            $('.cookie-configurator').toggleClass('active');
            $('.cookie-popup').removeClass('active');
            setInitialCookiePreferences();
        });
    }
});